# Context

[**Context**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/context/Context.java) is an interface which provides next methods:
- `TelegramClient getTelegramClient()` - get implementation of [`TelegramClient`](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/TelegramClient.java)
- `void setTelegramClient(TelegramClient)`
- `Update getUpdate()` - get current update
- `Context createNewContext(TelegramClient, Update)`

***

[**MethodsContext**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/context/MethodsContext.java)
```groovy
update {
	// this closure delegated to MethodsContext

	getTelegramClient() // or simply `telegramClient`

	getUpdate() // or simply `update`
	
	// etc..
}
```

[**MessageContext**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/context/MessageContext.java)
```groovy
message(~/mesg/) {
	getMatcher() // or simply `matcher`
}
```

[**CommandContext**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/context/CommandContext.java)
```groovy
command(~/echo/, ~/(.+)/) {
	getMatcher() // or simply `matcher`

	getArgsMatcher()

	getArgsText()
}
```


```groovy
import tech.teslex.telegroo.api.context.MethodsContext
import tech.teslex.telegroo.simple.update.SimpleCommandUpdateHandler
import tech.teslex.telegroo.simple.SimpleTelegroo

import java.util.regex.Pattern

class StartCommandHandler implements SimpleCommandUpdateHandler {
	
	Pattern pattern = ~/start/
	
	void handle(MethodsContext context) {
		context.sendMessage {
			text = 'Welcome!'
		}
	}
}

def bot = new SimpleTelegroo('TOKEN')

bot.commandUpdateHandler(new StartCommandHandler())
```