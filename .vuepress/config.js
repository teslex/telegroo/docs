const config = {
	version: '1.2'
}


module.exports = {
	base: '/telegroo/',
	title: 'telegroo',
	themeConfig: {
		title: 'telegroo',
		logo: '/images/telegroo-small.png',
		nav: [
			{
				text: 'GitLab', link: 'https://gitlab.com/teslex/telegroo',
			}
		],
		sidebar: [
			'/',
			{
				title: 'Guide',
				collapsable: false,
				children: [
					'/get-started/',
					'/getting-updates/',
					'/context/',
				]
			}
		]
	},
	markdown: {
		extendMarkdown: md => {
			md.use(confPlugin, 'conf_conf', ['text', 'fence', 'code_inline'], config);
		}
	}
}

const confPlugin = (md, ruleName, tokenTypes, config) => {
	function scan(state) {

		let i, blkIdx, token;

		for (blkIdx = state.tokens.length - 1; blkIdx >= 0; blkIdx--) {

			token = state.tokens[blkIdx]
			if (token.content && tokenTypes.includes(token.type)) {
				Object.keys(config).map((key, index) => {
					token.content = token.content.replace(`{~${key}~}`, config[key])
				});
			}

			if (token.children) {
				for (i = token.children.length - 1; i >= 0; i--) {
					token = state.tokens[blkIdx].children[i]
					if (token.content && tokenTypes.includes(token.type)) {
						Object.keys(config).map((key, index) => {
							token.content = token.content.replace(`{~${key}~}`, config[key])
						});
					}
				}
			}
		}
	}

	md.core.ruler.push(ruleName, scan);
}