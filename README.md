---
home: true
heroText: telegroo
heroImage: /images/telegroo.png
tagline: Telegram Bot framework for Groovy
actionText: Get Started →
actionLink: /get-started/
---