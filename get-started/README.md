# Getting Started

**Telegroo is a [**Telegram Bot**](https://core.telegram.org/bots) framework for [**Groovy**](http://groovy-lang.org)**

## Installation

**Current Version:** - `{~version~}`

### Gradle

```groovy
compile 'tech.teslex.telegroo:telegroo:{~version~}'
```

### Maven

```xml
<dependency>
		<groupId>tech.teslex.telegroo</groupId>
		<artifactId>telegroo</artifactId>
		<version>{~version~}</version>
</dependency>
```

### Grapes

```groovy
@Grab(group = 'tech.teslex.telegroo', module = 'telegroo', version = '{~version~}')
```

## Example


### Groovy Script simple example
```groovy
@Grab(group = 'tech.teslex.telegroo', module = 'telegroo', version = '{~version~}')

import tech.teslex.telegroo.simple.SimpleTelegroo
import tech.teslex.telegroo.telegram.api.attach.*
import tech.teslex.telegroo.telegram.api.methods.objects.*
import tech.teslex.telegroo.telegram.api.types.*

def bot = new SimpleTelegroo('TOKEN')

bot.update {
	println(update)
}

bot.command('start') {
	sendMessage(text: 'Welcome!')
}
	
bot.command(~/echo/, ~/(.+)/) {
	if (argsMather.find()) sendMessage {
			text = argsMather.group(1)
	}
}

bot.message('pic') {
	def photo = SendPhotoMethodObject.builder()
					.photo(Attach.byUrl('https://assets.gitlab-static.net/uploads/-/system/project/avatar/7278100/telegroo.png'))
					.caption('telegroo')
					.build()

	sendPhoto(photo)
}

bot.message("let's go") {
	reply {
		sendMessage text: 'okey!'
	}

	// or
	reply().sendMessage(text: 'okey!')
} 

bot.start()
```

### More examples
- [Telegroo with Micronaut](https://gitlab.com/teslex/telegroo/micronaut-example)