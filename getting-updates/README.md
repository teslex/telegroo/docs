# Getting Updates

[// core.telegram.org/bots/api#getting-updates](https://core.telegram.org/bots/api#getting-updates)

## Method

* [**`GetUpdatesMethod`**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegram-api/src/main/groovy/tech/teslex/telegroo/telegram/api/methods/interfaces/GetUpdatesMethod.java)
* [**`GetUpdatesMethodObject`**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegram-api/src/main/groovy/tech/teslex/telegroo/telegram/api/methods/objects/GetUpdatesMethodObject.groovy)

```groovy
getUpdates() // or just updates
```

```groovy
getUpdates(offset: 0, limit: 10, timeout: 100: allowedUpdates: [UpdateType.MESSAGE])
```



```groovy
import tech.teslex.telegroo.telegram.methods.objects.GetUpdatesMethodObject

def data = GetUpdatesMethodObject.builder()
	.offset(0)
	.limit(10)
	.timeout(100)
	.build()

getUpdates(data)
```

## Handlers

Telegroo provides 3 types of update handlers:

### UpdateHandler
[**`UpdateHandler<T extends Context>`**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/update/UpdateHandler.java) is the parent interface for update handlers.

```groovy
import tech.teslex.telegroo.api.update.UpdateHandler
import tech.teslex.telegroo.api.context.MethodsContext

class SomeUpdateHandler implements UpdateHandler<MethodsContext> {
	
	void handle(MethodsContext context) {
		println context.update
	}
}
```
### MessageUpdateHandler
[**`MessageUpdateHandler<T extends Context>`**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/update/MessageUpdateHandler.java) encapsulates `UpdateHandler` and used for handling message updates which match pattern.

```groovy
import tech.teslex.telegroo.api.update.MessageUpdateHandler
import tech.teslex.telegroo.api.context.MethodsContext

import java.util.regex.Pattern

class HeyMessageUpdateHandler implements MessageUpdateHandler<MethodsContext> /* SimpleMessageUpdateHandler */ {

	Pattern pattern = ~/hey/
	
	void handle(MethodsContext context) {
		context.sendMessage {
			text = 'Hey!'
		}
	}
}
```
### CommandUpdateHandler
[**`CommandUpdateHandler<T extends Context>`**](https://gitlab.com/teslex/telegroo/telegroo/tree/master/telegroo-api/src/main/groovy/tech/teslex/telegroo/api/update/CommandUpdateHandler.java) encapsulates `MessageUpdateHandler` and used for handling commands.

```groovy
import tech.teslex.telegroo.api.update.CommandUpdateHandler
import tech.teslex.telegroo.api.context.MethodsContext

import java.util.regex.Pattern

class StartCommandUpdateHandler implements CommandUpdateHandler<MethodsContext> /* SimpleCommandUpdateHandler */ {

	Pattern pattern = ~/start/
	
	void handle(MethodsContext context) {
		context.sendMessage {
			text = 'Welcome!'
		}
	}
}
```

## Handling

### Updates

```groovy
// on any update
update {
	println(update)
}
```
---

```groovy
update('edited_message') {
	println(update)
}
```
```groovy
import tech.teslex.telegroo.telegram.enums.UpdateType

update(UpdateType.EDITED_MESSAGE) {
	println(update)
}
```

### Messages and Commands
```groovy
message {
	println('New message!')
}
```

```groovy
message(/go/) {
	reply().sendMessage(text: 'go go go!')
}
```

```groovy
command(/start/) {
	sendMessage(text: 'Welcome!')
}
```

## Supported update types

- `message`: New incoming message of any kind — text, photo, sticker, etc.
- `edited_message`: New version of a message that is known to the bot and was edited
- `channel_post`: New incoming channel post of any kind — text, photo, sticker, etc.
- `edited_channel_post`: New version of a channel post that is known to the bot and was edited
- `inline_query`: New incoming inline query
- `chosen_inline_result`: The result of an inline query that was chosen by a user and sent to their chat partner. Please see our documentation on the feedback collecting for details on how to enable these updates for your 
- `callback_query`: New incoming callback query
- `shipping_query`: New incoming shipping query. Only for invoices with flexible price
- `pre_checkout_query`: New incoming pre-checkout query. Contains full information about checkout
